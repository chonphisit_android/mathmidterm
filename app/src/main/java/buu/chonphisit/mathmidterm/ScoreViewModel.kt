package buu.chonphisit.mathmidterm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScoreViewModel (correct: Int, incorrect: Int ) : ViewModel() {
    private var _correct = MutableLiveData<Int>()
    val correct :LiveData<Int>
    get() = _correct
    private var _incorrect = MutableLiveData<Int>()
    val incorrect :LiveData<Int>
        get() = _incorrect
    init {
        _correct.value = correct
        _incorrect.value = incorrect
        Log.i("ScoreViewModel", "Final score is $correct,")
        Log.i("ScoreViewModel", "Final score1 is $incorrect,")
    }
}