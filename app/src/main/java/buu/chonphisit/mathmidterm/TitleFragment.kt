package buu.chonphisit.mathmidterm

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.chonphisit.mathmidterm.databinding.FragmentTitleBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TitleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TitleFragment : Fragment() {

    private lateinit var viewModel: ScoreViewModel
    private lateinit var viewModelFactory: ScoreViewModelFactory
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater, R.layout.fragment_title, container, false)

        viewModelFactory = ScoreViewModelFactory(
            TitleFragmentArgs.fromBundle(requireArguments()).correct,
            TitleFragmentArgs.fromBundle(requireArguments()).incorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(ScoreViewModel::class.java)
        //binding.score?.mycorrect = viewModel.score.value!!
        //binding.score?.myincorrect = viewModel.score1.value!!

        val args = TitleFragmentArgs.fromBundle(requireArguments())
        Toast.makeText(context, "NumCorrect: ${viewModel.correct.value}, NumQuestions: ${viewModel.incorrect.value}", Toast.LENGTH_LONG).show()
        viewModel.correct.value?.plus(args.correct)
        viewModel.incorrect.value?.plus(args.incorrect)
        Log.i("mycorrect", "${viewModel.correct} || ${viewModel.incorrect} ")


        binding.btnplus.setOnClickListener {view ->
            val action = TitleFragmentDirections.actionTitleFragmentToPlusFragment(
                viewModel.correct.value!!,viewModel.incorrect.value!!
            )
            view.findNavController().navigate(action)
        }

        binding.btnminus.setOnClickListener {view ->
            val action = TitleFragmentDirections.actionTitleFragmentToMinusFragment(
                viewModel.correct.value!!,viewModel.incorrect.value!!
            )
            view.findNavController().navigate(action)
//            view.findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToMinusFragment())
        }

        binding.btnmultiply.setOnClickListener {view ->
            val action = TitleFragmentDirections.actionTitleFragmentToMultipleFragment(
                viewModel.correct.value!!,viewModel.incorrect.value!!
            )
            view.findNavController().navigate(action)
//            view.findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToMultipleFragment())
        }
        setHasOptionsMenu(true)
        binding.scoreViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            requireView().findNavController()) || super.onOptionsItemSelected(item)
    }


}