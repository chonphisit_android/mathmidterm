package buu.chonphisit.mathmidterm

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class MultipleViewModel : ViewModel() {
    private val score: Score = Score()

    private var _num1 = MutableLiveData<Int>()
    val num1: LiveData<Int>
        get() = _num1

    private var _num2 = MutableLiveData<Int>()
    val num2: LiveData<Int>
        get() = _num2

    private var _btn1 = MutableLiveData<Int>()
    val btn1: LiveData<Int>
        get() = _btn1

    private var _btn2 = MutableLiveData<Int>()
    val btn2: LiveData<Int>
        get() = _btn2

    private var _btn3 = MutableLiveData<Int>()
    val btn3: LiveData<Int>
        get() = _btn3

    var randomnum = Random.nextInt(0, 10)
    var randomnum1 = Random.nextInt(0, 10)
    var answer = (randomnum * randomnum1)
    val choice = Random.nextInt(0, 3)
    private var _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct
    private var _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    fun setCorrect(correct:Int){
        _correct.value = correct
    }
    fun setIncorrect(incorrect:Int){
        _incorrect.value = incorrect
    }

    init {
        _num1.value = 0
        _num2.value = 0
        _btn1.value = 0
        _btn2.value = 0
        _btn3.value = 0
        _correct.value = 0
        _incorrect.value = 0
        start()
        Log.i("PlusViewModel", "PlusViewModel created!")
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("PlusViewModel", "PlusViewModel destroyed!")
    }

    fun start() {
        randomnum = Random.nextInt(0, 10)
        randomnum1 = Random.nextInt(0, 10)
        _num1.value = randomnum
        _num2.value = randomnum1
        answer = (randomnum * randomnum1)


        if (choice == 0) {
            _btn1.value = answer
            _btn2.value = (answer + 1)
            _btn3.value = (answer - 1)
        } else if (choice == 1) {
            _btn1.value = (answer + 1)
            _btn2.value = answer
            _btn3.value = (answer - 1)
        } else {
            _btn1.value = (answer - 1)
            _btn2.value = (answer + 1)
            _btn3.value = answer

        }

    }


    fun checkbutton(btn: Int) {
        Log.i("value","${_incorrect.value} || ${_correct.value}")
        if (btn == answer) {
            _correct.value = (_correct.value)?.plus(1)
            score.mycorrect = score.mycorrect.plus(1)
            start()

        } else {
            _incorrect.value = (_incorrect.value)?.plus(1)
            score.myincorrect = score.myincorrect.plus(1)
            start()
        }
    }
}