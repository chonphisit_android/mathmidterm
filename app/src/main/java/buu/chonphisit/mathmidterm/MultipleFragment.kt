package buu.chonphisit.mathmidterm

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.chonphisit.mathmidterm.databinding.FragmentMultipleBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MultipleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MultipleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    private lateinit var binding: FragmentMultipleBinding
    private val score:Score = Score()

    private lateinit var viewModel: MultipleViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_multiple, container, false)

        Log.i("MultipleFragment", "Call ViewModelProviders.of")
        viewModel = ViewModelProvider(this).get(MultipleViewModel::class.java)
        val args = MultipleFragmentArgs.fromBundle(requireArguments())
        Log.i("viewmodel:", "${args.multiplewin} | ${args.multiplewrong}")
        viewModel.setCorrect(args.multiplewin)
        viewModel.setIncorrect(args.multiplewrong)
        binding.btnBack.setOnClickListener { view ->
            val thisView = MultipleFragmentDirections.actionMultipleFragmentToTitleFragment(
                viewModel.correct.value!!,viewModel.incorrect.value!!
            )
            view.findNavController().navigate(thisView)
        }
        requireActivity().onBackPressedDispatcher.addCallback(this){
            view?.findNavController()?.navigate(MultipleFragmentDirections.actionMultipleFragmentToTitleFragment(viewModel.correct.value!!, viewModel
                .incorrect.value!!))
        }
        binding.multipleViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root

    }



    private fun ansIncorrect(textalert: TextView?) {
        textalert!!.text = ("Incorrect")
        textalert.setTextColor(Color.RED)
    }

    private fun ansCorrect(textalert: TextView?) {
        textalert!!.text = ("Correct")
        textalert.setTextColor(Color.GREEN)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MultipleFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MultipleFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}